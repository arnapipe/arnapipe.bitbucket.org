sample_id	N_unmapped	N_multimapping	N_noFeature	N_ambiguous	TotalCounts	Link
P1N	145924	1866105	648147	334064	5978261	<a href="../results_star/P1N_ReadsPerGene.out.tab" target="_blank">+</a>
P1T	133427	1364685	330403	520731	6098536	<a href="../results_star/P1T_ReadsPerGene.out.tab" target="_blank">+</a>
P2N	223205	2064126	445104	666983	7734346	<a href="../results_star/P2N_ReadsPerGene.out.tab" target="_blank">+</a>
P2T	159756	2038757	498014	541513	8096836	<a href="../results_star/P2T_ReadsPerGene.out.tab" target="_blank">+</a>
