PASS	Basic Statistics	SRR222178.fastq.gz
PASS	Per base sequence quality	SRR222178.fastq.gz
PASS	Per sequence quality scores	SRR222178.fastq.gz
FAIL	Per base sequence content	SRR222178.fastq.gz
FAIL	Per base GC content	SRR222178.fastq.gz
PASS	Per sequence GC content	SRR222178.fastq.gz
PASS	Per base N content	SRR222178.fastq.gz
WARN	Sequence Length Distribution	SRR222178.fastq.gz
WARN	Sequence Duplication Levels	SRR222178.fastq.gz
PASS	Overrepresented sequences	SRR222178.fastq.gz
WARN	Kmer Content	SRR222178.fastq.gz
