sample_id	N_unmapped	N_multimapping	N_noFeature	N_ambiguous	TotalCounts	Link
BRAIN_1	2039258	3746186	3099597	4799421	60098505	<a href="../results_star/BRAIN_1_ReadsPerGene.out.tab" target="_blank">+</a>
BRAIN_2	507860	6455569	4715652	3794254	47326571	<a href="../results_star/BRAIN_2_ReadsPerGene.out.tab" target="_blank">+</a>
HT_1	647373	3812123	3988880	5010672	57424748	<a href="../results_star/HT_1_ReadsPerGene.out.tab" target="_blank">+</a>
HT_2	1645220	2881098	7234006	5720048	65162030	<a href="../results_star/HT_2_ReadsPerGene.out.tab" target="_blank">+</a>
LV_1	273881	11070473	2394326	4425186	49764514	<a href="../results_star/LV_1_ReadsPerGene.out.tab" target="_blank">+</a>
LV_2	1928380	4746847	7792317	5524804	57714608	<a href="../results_star/LV_2_ReadsPerGene.out.tab" target="_blank">+</a>
TS_1	429222	3277173	8930283	4573339	51042289	<a href="../results_star/TS_1_ReadsPerGene.out.tab" target="_blank">+</a>
TS_2	2798386	2904030	16963460	4865254	57842112	<a href="../results_star/TS_2_ReadsPerGene.out.tab" target="_blank">+</a>
TS_1_REP	1987510	1612614	8941266	249791	3468337	<a href="../results_star/TS_1_REP_ReadsPerGene.out.tab" target="_blank">+</a>
