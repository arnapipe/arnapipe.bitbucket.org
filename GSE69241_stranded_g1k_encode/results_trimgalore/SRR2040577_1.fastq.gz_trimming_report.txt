
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/home/aalonso/test_datasets/GSE69241/fastq/SRR2040577_1.fastq.gz
Trimming mode: paired-end
Trim Galore version: 0.4.1
Cutadapt version: 1.8.1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'TGGAATTCTCGG' (Illumina small RNA adapter; auto-detected)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length for both reads before a sequence pair gets removed: 16 bp
Output file will be GZIP compressed


This is cutadapt 1.8.1 with Python 2.7.2
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a TGGAATTCTCGG /gpfs/gpfs1/home/aalonso/test_datasets/GSE69241/fastq/SRR2040577_1.fastq.gz
Trimming 1 adapter with at most 10.0% errors in single-end mode ...
Finished in 2271.50 s (32 us/read; 1.87 M reads/minute).

=== Summary ===

Total reads processed:              70,846,205
Reads with adapters:                24,315,463 (34.3%)
Reads written (passing filters):    70,846,205 (100.0%)

Total basepairs processed: 6,929,514,430 bp
Quality-trimmed:                  23,811 bp (0.0%)
Total written (filtered):  6,894,380,249 bp (99.5%)

=== Adapter 1 ===

Sequence: TGGAATTCTCGG; Type: regular 3'; Length: 12; Trimmed: 24315463 times.

No. of allowed errors:
0-9 bp: 0; 10-12 bp: 1

Bases preceding removed adapters:
  A: 23.2%
  C: 29.4%
  G: 16.7%
  T: 30.7%
  none/other: 0.0%

Overview of removed sequences
length	count	expect	max.err	error counts
1	16986767	17711551.2	0	16986767
2	5103944	4427887.8	0	5103944
3	1746617	1106972.0	0	1746617
4	345787	276743.0	0	345787
5	90805	69185.7	0	90805
6	19170	17296.4	0	19170
7	5568	4324.1	0	5568
8	1042	1081.0	0	1042
9	2093	270.3	0	282 1811
10	1892	67.6	1	118 1774
11	906	16.9	1	2 904
12	156	4.2	1	3 153
13	100	4.2	1	1 99
14	120	4.2	1	3 117
15	78	4.2	1	4 74
16	104	4.2	1	6 98
17	94	4.2	1	3 91
18	94	4.2	1	4 90
19	84	4.2	1	0 84
20	103	4.2	1	2 101
21	110	4.2	1	5 105
22	85	4.2	1	0 85
23	66	4.2	1	2 64
24	108	4.2	1	1 107
25	104	4.2	1	8 96
26	122	4.2	1	7 115
27	102	4.2	1	4 98
28	113	4.2	1	12 101
29	140	4.2	1	4 136
30	122	4.2	1	8 114
31	365	4.2	1	5 360
32	216	4.2	1	0 216
33	94	4.2	1	1 93
34	119	4.2	1	1 118
35	152	4.2	1	1 151
36	110	4.2	1	2 108
37	135	4.2	1	6 129
38	117	4.2	1	10 107
39	116	4.2	1	3 113
40	129	4.2	1	9 120
41	131	4.2	1	3 128
42	107	4.2	1	0 107
43	106	4.2	1	5 101
44	155	4.2	1	7 148
45	99	4.2	1	4 95
46	183	4.2	1	5 178
47	215	4.2	1	4 211
48	148	4.2	1	9 139
49	90	4.2	1	1 89
50	82	4.2	1	1 81
51	79	4.2	1	2 77
52	82	4.2	1	1 81
53	85	4.2	1	3 82
54	99	4.2	1	2 97
55	92	4.2	1	1 91
56	124	4.2	1	3 121
57	151	4.2	1	3 148
58	138	4.2	1	4 134
59	86	4.2	1	2 84
60	97	4.2	1	2 95
61	127	4.2	1	0 127
62	121	4.2	1	1 120
63	89	4.2	1	3 86
64	113	4.2	1	3 110
65	102	4.2	1	1 101
66	90	4.2	1	2 88
67	94	4.2	1	5 89
68	82	4.2	1	4 78
69	117	4.2	1	1 116
70	128	4.2	1	1 127
71	93	4.2	1	2 91
72	136	4.2	1	2 134
73	64	4.2	1	4 60
74	90	4.2	1	1 89
75	103	4.2	1	1 102
76	97	4.2	1	2 95
77	108	4.2	1	1 107
78	93	4.2	1	4 89
79	103	4.2	1	1 102
80	76	4.2	1	0 76
81	112	4.2	1	0 112
82	127	4.2	1	2 125
83	98	4.2	1	4 94
84	65	4.2	1	3 62
85	138	4.2	1	6 132
86	97	4.2	1	1 96
87	96	4.2	1	2 94
88	123	4.2	1	2 121
89	111	4.2	1	2 109
90	71	4.2	1	2 69
91	112	4.2	1	7 105
92	85	4.2	1	4 81
93	98	4.2	1	3 95
94	113	4.2	1	8 105
95	99	4.2	1	3 96
96	92	4.2	1	3 89
97	205	4.2	1	2 203
98	271	4.2	1	16 255
99	765	4.2	1	5 760
100	61	4.2	1	4 57


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/home/aalonso/test_datasets/GSE69241/fastq/SRR2040577_1.fastq.gz
=============================================
70846205 sequences processed in total

