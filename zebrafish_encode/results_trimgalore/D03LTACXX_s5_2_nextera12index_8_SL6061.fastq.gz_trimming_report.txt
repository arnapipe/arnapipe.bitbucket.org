
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/myerslab/data/Libraries/SL6061/D03LTACXX_s5_2_nextera12index_8_SL6061.fastq.gz
Trimming mode: paired-end
Trim Galore version: 0.4.1
Cutadapt version: 1.8.1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'CTGTCTCTTATA' (Nextera Transposase sequence; auto-detected)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length for both reads before a sequence pair gets removed: 20 bp
Output file will be GZIP compressed


This is cutadapt 1.8.1 with Python 2.7.2
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a CTGTCTCTTATA /gpfs/gpfs1/myerslab/data/Libraries/SL6061/D03LTACXX_s5_2_nextera12index_8_SL6061.fastq.gz
Trimming 1 adapter with at most 10.0% errors in single-end mode ...
Finished in 618.89 s (16 us/read; 3.73 M reads/minute).

=== Summary ===

Total reads processed:              38,519,014
Reads with adapters:                13,024,178 (33.8%)
Reads written (passing filters):    38,519,014 (100.0%)

Total basepairs processed: 1,925,950,700 bp
Quality-trimmed:             133,042,322 bp (6.9%)
Total written (filtered):  1,773,149,619 bp (92.1%)

=== Adapter 1 ===

Sequence: CTGTCTCTTATA; Type: regular 3'; Length: 12; Trimmed: 13024178 times.

No. of allowed errors:
0-9 bp: 0; 10-12 bp: 1

Bases preceding removed adapters:
  A: 22.1%
  C: 25.1%
  G: 24.9%
  T: 27.8%
  none/other: 0.0%

Overview of removed sequences
length	count	expect	max.err	error counts
1	9120328	9629753.5	0	9120328
2	2529185	2407438.4	0	2529185
3	995680	601859.6	0	995680
4	203805	150464.9	0	203805
5	58282	37616.2	0	58282
6	23283	9404.1	0	23283
7	11410	2351.0	0	11410
8	8949	587.8	0	8949
9	10760	146.9	0	9966 794
10	8029	36.7	1	5393 2636
11	7923	9.2	1	6416 1507
12	6792	2.3	1	5345 1447
13	6177	2.3	1	4727 1450
14	5037	2.3	1	3363 1674
15	3057	2.3	1	2346 711
16	2841	2.3	1	1570 1271
17	1956	2.3	1	1326 630
18	1760	2.3	1	1164 596
19	1749	2.3	1	968 781
20	1373	2.3	1	902 471
21	2119	2.3	1	1225 894
22	1778	2.3	1	1384 394
23	1278	2.3	1	887 391
24	1414	2.3	1	254 1160
25	190	2.3	1	47 143
26	361	2.3	1	37 324
27	226	2.3	1	34 192
28	440	2.3	1	26 414
29	321	2.3	1	33 288
30	1099	2.3	1	30 1069
31	452	2.3	1	18 434
32	501	2.3	1	12 489
33	246	2.3	1	11 235
34	226	2.3	1	19 207
35	201	2.3	1	3 198
36	200	2.3	1	13 187
37	233	2.3	1	6 227
38	306	2.3	1	7 299
39	167	2.3	1	6 161
40	350	2.3	1	6 344
41	277	2.3	1	5 272
42	2185	2.3	1	5 2180
43	465	2.3	1	0 465
44	117	2.3	1	4 113
45	41	2.3	1	1 40
46	62	2.3	1	0 62
47	292	2.3	1	5 287
48	168	2.3	1	0 168
49	39	2.3	1	0 39
50	48	2.3	1	0 48


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/myerslab/data/Libraries/SL6061/D03LTACXX_s5_2_nextera12index_8_SL6061.fastq.gz
=============================================
38519014 sequences processed in total

Total number of sequences analysed for the sequence pair length validation: 38519014

Number of sequence pairs removed because at least one read was shorter than the length cutoff (20 bp): 6770698 (17.58%)
