PASS	Basic Statistics	SRR222175.fastq.gz
PASS	Per base sequence quality	SRR222175.fastq.gz
PASS	Per sequence quality scores	SRR222175.fastq.gz
FAIL	Per base sequence content	SRR222175.fastq.gz
FAIL	Per base GC content	SRR222175.fastq.gz
WARN	Per sequence GC content	SRR222175.fastq.gz
PASS	Per base N content	SRR222175.fastq.gz
WARN	Sequence Length Distribution	SRR222175.fastq.gz
WARN	Sequence Duplication Levels	SRR222175.fastq.gz
WARN	Overrepresented sequences	SRR222175.fastq.gz
FAIL	Kmer Content	SRR222175.fastq.gz
