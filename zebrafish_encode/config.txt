%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
trimgalore	20/NA/NA
fastqc	10/NA/NA
kallisto	10/NA/NA
star	12/40/4
star-fusion	0/NA/NA
picard	10/NA/NA
htseq-gene	10/NA/NA
htseq-exon	10/NA/NA
sam2sortbam	10/NA/NA
picard_IS	10/NA/NA
varscan	10/NA/NA
gatk	0/NA/NA
jsplice	0/NA/NA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% GENOMIC REFERENCES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
genome_build	ZF_GRCz10_E83
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% HPC CONFIG
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
wt	250:00
q	normal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ADDITIONAL PROGRAM OPTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
kalboot	20
trimgal_args	
star2pass	yes
star_args	default
star_args_own	
starfusion_args	default
varscan_args	--output-vcf 1 --variants 1
gatk_args	
strandedness	no
htseq-gene-mode	union
htseq-exon-mode	union
jsplice_pheno	
jsplice_args	

