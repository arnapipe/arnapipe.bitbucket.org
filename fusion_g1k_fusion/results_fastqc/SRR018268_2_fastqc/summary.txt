PASS	Basic Statistics	SRR018268_2.fastq.gz
FAIL	Per base sequence quality	SRR018268_2.fastq.gz
FAIL	Per sequence quality scores	SRR018268_2.fastq.gz
WARN	Per base sequence content	SRR018268_2.fastq.gz
FAIL	Per base GC content	SRR018268_2.fastq.gz
PASS	Per sequence GC content	SRR018268_2.fastq.gz
PASS	Per base N content	SRR018268_2.fastq.gz
PASS	Sequence Length Distribution	SRR018268_2.fastq.gz
WARN	Sequence Duplication Levels	SRR018268_2.fastq.gz
FAIL	Overrepresented sequences	SRR018268_2.fastq.gz
WARN	Kmer Content	SRR018268_2.fastq.gz
