
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/myerslab/data/Libraries/SL6054/D03LTACXX_s2_2_nextera12index_1_SL6054.fastq.gz
Trimming mode: paired-end
Trim Galore version: 0.4.1
Cutadapt version: 1.8.1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'CTGTCTCTTATA' (Nextera Transposase sequence; auto-detected)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length for both reads before a sequence pair gets removed: 20 bp
Output file will be GZIP compressed


This is cutadapt 1.8.1 with Python 2.7.2
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a CTGTCTCTTATA /gpfs/gpfs1/myerslab/data/Libraries/SL6054/D03LTACXX_s2_2_nextera12index_1_SL6054.fastq.gz
Trimming 1 adapter with at most 10.0% errors in single-end mode ...
Finished in 577.03 s (16 us/read; 3.82 M reads/minute).

=== Summary ===

Total reads processed:              36,715,574
Reads with adapters:                12,332,592 (33.6%)
Reads written (passing filters):    36,715,574 (100.0%)

Total basepairs processed: 1,835,778,700 bp
Quality-trimmed:              72,104,608 bp (3.9%)
Total written (filtered):  1,744,408,206 bp (95.0%)

=== Adapter 1 ===

Sequence: CTGTCTCTTATA; Type: regular 3'; Length: 12; Trimmed: 12332592 times.

No. of allowed errors:
0-9 bp: 0; 10-12 bp: 1

Bases preceding removed adapters:
  A: 22.6%
  C: 24.6%
  G: 24.8%
  T: 28.0%
  none/other: 0.0%

Overview of removed sequences
length	count	expect	max.err	error counts
1	8538641	9178893.5	0	8538641
2	2421285	2294723.4	0	2421285
3	943384	573680.8	0	943384
4	209137	143420.2	0	209137
5	62543	35855.1	0	62543
6	27553	8963.8	0	27553
7	16915	2240.9	0	16915
8	13165	560.2	0	13165
9	15112	140.1	0	14283 829
10	11701	35.0	1	9602 2099
11	12498	8.8	1	10944 1554
12	10247	2.2	1	9056 1191
13	9354	2.2	1	8397 957
14	7054	2.2	1	5849 1205
15	4820	2.2	1	4216 604
16	3901	2.2	1	2748 1153
17	2817	2.2	1	2361 456
18	2399	2.2	1	1926 473
19	2334	2.2	1	1612 722
20	1872	2.2	1	1532 340
21	2596	2.2	1	1798 798
22	2206	2.2	1	1813 393
23	1388	2.2	1	1062 326
24	1253	2.2	1	320 933
25	187	2.2	1	59 128
26	361	2.2	1	45 316
27	241	2.2	1	47 194
28	392	2.2	1	20 372
29	289	2.2	1	31 258
30	820	2.2	1	25 795
31	368	2.2	1	23 345
32	269	2.2	1	8 261
33	187	2.2	1	15 172
34	204	2.2	1	9 195
35	164	2.2	1	11 153
36	192	2.2	1	8 184
37	211	2.2	1	2 209
38	270	2.2	1	2 268
39	179	2.2	1	8 171
40	552	2.2	1	3 549
41	267	2.2	1	4 263
42	2024	2.2	1	4 2020
43	395	2.2	1	0 395
44	129	2.2	1	1 128
45	94	2.2	1	1 93
46	95	2.2	1	1 94
47	276	2.2	1	3 273
48	169	2.2	1	1 168
49	46	2.2	1	0 46
50	36	2.2	1	0 36


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/myerslab/data/Libraries/SL6054/D03LTACXX_s2_2_nextera12index_1_SL6054.fastq.gz
=============================================
36715574 sequences processed in total

Total number of sequences analysed for the sequence pair length validation: 36715574

Number of sequence pairs removed because at least one read was shorter than the length cutoff (20 bp): 6090974 (16.59%)
