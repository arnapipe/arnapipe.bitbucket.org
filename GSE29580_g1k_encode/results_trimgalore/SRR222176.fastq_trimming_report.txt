
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222176.fastq
Trimming mode: single-end
Trim Galore version: 0.4.1
Cutadapt version: 1.8.1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'AGATCGGAAGAGC' (Illumina TruSeq, Sanger iPCR; auto-detected)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length before a sequence gets removed: 20 bp
Output file will be GZIP compressed


This is cutadapt 1.8.1 with Python 2.7.2
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a AGATCGGAAGAGC /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222176.fastq
Trimming 1 adapter with at most 10.0% errors in single-end mode ...
Finished in 247.74 s (29 us/read; 2.07 M reads/minute).

=== Summary ===

Total reads processed:               8,542,144
Reads with adapters:                 2,568,130 (30.1%)
Reads written (passing filters):     8,542,144 (100.0%)

Total basepairs processed:   555,239,360 bp
Quality-trimmed:              98,835,769 bp (17.8%)
Total written (filtered):    452,214,961 bp (81.4%)

=== Adapter 1 ===

Sequence: AGATCGGAAGAGC; Type: regular 3'; Length: 13; Trimmed: 2568130 times.

No. of allowed errors:
0-9 bp: 0; 10-13 bp: 1

Bases preceding removed adapters:
  A: 25.5%
  C: 42.0%
  G: 18.8%
  T: 13.3%
  none/other: 0.3%

Overview of removed sequences
length	count	expect	max.err	error counts
1	1815718	2135536.0	0	1815718
2	569187	533884.0	0	569187
3	121556	133471.0	0	121556
4	25147	33367.8	0	25147
5	6762	8341.9	0	6762
6	1896	2085.5	0	1896
7	1327	521.4	0	1327
8	814	130.3	0	814
9	1565	32.6	0	1459 106
10	1307	8.1	1	1148 159
11	1368	2.0	1	1244 124
12	1126	0.5	1	1075 51
13	565	0.1	1	538 27
14	991	0.1	1	945 46
15	1662	0.1	1	1609 53
16	389	0.1	1	360 29
17	962	0.1	1	929 33
18	583	0.1	1	546 37
19	846	0.1	1	809 37
20	640	0.1	1	611 29
21	390	0.1	1	363 27
22	581	0.1	1	546 35
23	536	0.1	1	495 41
24	516	0.1	1	472 44
25	449	0.1	1	305 144
26	922	0.1	1	751 171
27	620	0.1	1	510 110
28	358	0.1	1	255 103
29	317	0.1	1	188 129
30	295	0.1	1	215 80
31	423	0.1	1	273 150
32	481	0.1	1	317 164
33	273	0.1	1	221 52
34	309	0.1	1	245 64
35	192	0.1	1	128 64
36	170	0.1	1	86 84
37	221	0.1	1	113 108
38	166	0.1	1	58 108
39	183	0.1	1	71 112
40	161	0.1	1	30 131
41	218	0.1	1	53 165
42	216	0.1	1	36 180
43	173	0.1	1	28 145
44	219	0.1	1	11 208
45	187	0.1	1	17 170
46	183	0.1	1	17 166
47	183	0.1	1	6 177
48	194	0.1	1	3 191
49	199	0.1	1	2 197
50	214	0.1	1	11 203
51	217	0.1	1	4 213
52	269	0.1	1	1 268
53	212	0.1	1	3 209
54	246	0.1	1	8 238
55	299	0.1	1	1 298
56	374	0.1	1	1 373
57	476	0.1	1	5 471
58	460	0.1	1	0 460
59	300	0.1	1	0 300
60	358	0.1	1	1 357
61	318	0.1	1	0 318
62	323	0.1	1	0 323
63	189	0.1	1	0 189
64	79	0.1	1	0 79
65	50	0.1	1	0 50


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/home/aalonso/article/colorectal_GSE29580/fastq/SRR222176.fastq
=============================================
8542144 sequences processed in total
Sequences removed because they became shorter than the length cutoff of 20 bp:	94362 (1.1%)

