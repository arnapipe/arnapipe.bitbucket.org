
SUMMARISING RUN PARAMETERS
==========================
Input filename: /gpfs/gpfs1/myerslab/data/Libraries/SL6065/D03LTACXX_s7_2_nextera12index_12_SL6065.fastq.gz
Trimming mode: paired-end
Trim Galore version: 0.4.1
Cutadapt version: 1.8.1
Quality Phred score cutoff: 20
Quality encoding type selected: ASCII+33
Adapter sequence: 'CTGTCTCTTATA' (Nextera Transposase sequence; auto-detected)
Maximum trimming error rate: 0.1 (default)
Minimum required adapter overlap (stringency): 1 bp
Minimum required sequence length for both reads before a sequence pair gets removed: 20 bp
Output file will be GZIP compressed


This is cutadapt 1.8.1 with Python 2.7.2
Command line parameters: -f fastq -e 0.1 -q 20 -O 1 -a CTGTCTCTTATA /gpfs/gpfs1/myerslab/data/Libraries/SL6065/D03LTACXX_s7_2_nextera12index_12_SL6065.fastq.gz
Trimming 1 adapter with at most 10.0% errors in single-end mode ...
Finished in 613.45 s (22 us/read; 2.72 M reads/minute).

=== Summary ===

Total reads processed:              27,780,077
Reads with adapters:                 9,513,391 (34.2%)
Reads written (passing filters):    27,780,077 (100.0%)

Total basepairs processed: 1,389,003,850 bp
Quality-trimmed:              65,790,038 bp (4.7%)
Total written (filtered):  1,308,622,950 bp (94.2%)

=== Adapter 1 ===

Sequence: CTGTCTCTTATA; Type: regular 3'; Length: 12; Trimmed: 9513391 times.

No. of allowed errors:
0-9 bp: 0; 10-12 bp: 1

Bases preceding removed adapters:
  A: 22.1%
  C: 25.0%
  G: 25.1%
  T: 27.8%
  none/other: 0.0%

Overview of removed sequences
length	count	expect	max.err	error counts
1	6638447	6945019.2	0	6638447
2	1862782	1736254.8	0	1862782
3	715124	434063.7	0	715124
4	155079	108515.9	0	155079
5	42611	27129.0	0	42611
6	18483	6782.2	0	18483
7	9732	1695.6	0	9732
8	7761	423.9	0	7761
9	9241	106.0	0	8696 545
10	6944	26.5	1	5575 1369
11	7434	6.6	1	6433 1001
12	6328	1.7	1	5466 862
13	5514	1.7	1	4762 752
14	4442	1.7	1	3472 970
15	2796	1.7	1	2367 429
16	2531	1.7	1	1601 930
17	1641	1.7	1	1319 322
18	1361	1.7	1	1021 340
19	1622	1.7	1	999 623
20	1172	1.7	1	943 229
21	1798	1.7	1	1171 627
22	1402	1.7	1	1129 273
23	1083	1.7	1	819 264
24	1205	1.7	1	237 968
25	145	1.7	1	63 82
26	271	1.7	1	37 234
27	164	1.7	1	38 126
28	329	1.7	1	22 307
29	216	1.7	1	22 194
30	842	1.7	1	30 812
31	319	1.7	1	12 307
32	230	1.7	1	9 221
33	144	1.7	1	10 134
34	203	1.7	1	20 183
35	127	1.7	1	7 120
36	131	1.7	1	11 120
37	162	1.7	1	3 159
38	240	1.7	1	5 235
39	134	1.7	1	2 132
40	270	1.7	1	1 269
41	216	1.7	1	5 211
42	1759	1.7	1	3 1756
43	326	1.7	1	1 325
44	94	1.7	1	1 93
45	49	1.7	1	0 49
46	47	1.7	1	0 47
47	240	1.7	1	2 238
48	130	1.7	1	0 130
49	25	1.7	1	0 25
50	45	1.7	1	0 45


RUN STATISTICS FOR INPUT FILE: /gpfs/gpfs1/myerslab/data/Libraries/SL6065/D03LTACXX_s7_2_nextera12index_12_SL6065.fastq.gz
=============================================
27780077 sequences processed in total

Total number of sequences analysed for the sequence pair length validation: 27780077

Number of sequence pairs removed because at least one read was shorter than the length cutoff (20 bp): 628872 (2.26%)
