PASS	Basic Statistics	SRR018259_1.fastq.gz
FAIL	Per base sequence quality	SRR018259_1.fastq.gz
FAIL	Per sequence quality scores	SRR018259_1.fastq.gz
WARN	Per base sequence content	SRR018259_1.fastq.gz
WARN	Per base GC content	SRR018259_1.fastq.gz
WARN	Per sequence GC content	SRR018259_1.fastq.gz
PASS	Per base N content	SRR018259_1.fastq.gz
PASS	Sequence Length Distribution	SRR018259_1.fastq.gz
WARN	Sequence Duplication Levels	SRR018259_1.fastq.gz
FAIL	Overrepresented sequences	SRR018259_1.fastq.gz
WARN	Kmer Content	SRR018259_1.fastq.gz
